from django.urls import path
from . import views

app_name = 'Js_JQuery'
urlpatterns = [
    path('',views.index, name = 'index'),
]