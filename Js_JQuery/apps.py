from django.apps import AppConfig


class JsJqueryConfig(AppConfig):
    name = 'Js_JQuery'
