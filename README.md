# Hans' Bio

**PPW Tugas 7 Project**


## Author

* Hans Michael Nabasa Pasaribu - 1806235662

## Herokuapp Link

[Hans' Bio](https://hansbio.herokuapp.com) 

## Project Details

Web ini berfungsi untuk menampilkan biodata Hans serta mengganti tema tampilan sesuai keinginan user

## Pipelines Status

[![pipeline status](https://gitlab.com/hansmichael.hm/tugas-7/badges/master/pipeline.svg)](https://gitlab.com/hansmichael.hm/tugas-7/commits/master)

## Coverage Status
[![coverage report](https://gitlab.com/hansmichael.hm/tugas-7/badges/master/coverage.svg)](https://gitlab.com/hansmichael.hm/tugas-7/commits/master)